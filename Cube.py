from enum import Enum

class Position(Enum):
    U = 0; L = 1; F = 2; R = 3; B = 4; D = 5


class Color(Enum):
    W = 0; O = 1; G = 2; R = 3; B = 4; Y = 5


class Piece:
    def isAt(self, positionSet):
        return positionSet.issubset(set(self.colors.values()))
    
class Face(Piece):

    def __init__(self, color, position):
        self.colors = {color: position}
        
    def __str__(self):
        return f"Face: {self.color}: {self.position}"

    
class EdgePosition(Enum):
    UL = 0; UF = 1; UR = 2; UB = 3 
    LB = 4; LF = 5; RF = 6; RB = 7
    DL = 8; DF = 9; DR = 10; DB = 11


class Edge(Piece):

    def __init__(self, position, color0, color1):
        self.colors = {
            Color[color0] : Position[position.name[0]],
            Color[color1] : Position[position.name[1]]
            }
    
    def colorAt(self, position):
        for key, value in self.colors.items():
            if value == position:
                return key.name
    
    def __str__(self):
        return str(self.colors)


class CornerPosition(Enum):
    ULB = 0; ULF = 1; URF = 2; URB = 3
    DLB = 4; DLF = 5; DRF = 6; DRB = 7


class Corner(Piece):

    def __init__(self, position, color0, color1, color2):
        self.colors = {
            Color[color0]: Position[position.name[0]],
            Color[color1]: Position[position.name[1]],
            Color[color2]: Position[position.name[2]]
            }

    def colorAt(self, position):
        for key, value in self.colors.items():
            if value == position:
                return key.name
    
    def __str__(self):
        return str(self.colors)


class Cube:
    
    def __init__(self, text=''):
        if(text == ''):
            text = 'WWWWWWWWWOOOOOOOOOGGGGGGGGGRRRRRRRRRBBBBBBBBBYYYYYYYYY'
        c = list(text)
        self.faces = [Face(Color[c[p.value * 9 + 4]], p) for p in list(Position)]

        edge = [(3, 10), (7, 19), (5, 28), (1, 37),
               (12, 41), (14, 21), (30, 23), (32, 39),
               (48, 16), (46, 25), (50, 34), (52, 43)]
        self.edges = [Edge(p, c[edge[p.value][0]], c[edge[p.value][1]]) for p in list(EdgePosition)]
        
        corn = [(0, 9, 38), (6, 11, 18), (8, 27, 20), (2, 29, 36),
                 (51, 15, 44), (45, 17, 24), (47, 33, 26), (53, 35, 42)]
        self.corns = [Corner(p, c[corn[p.value][0]], c[corn[p.value][1]], c[corn[p.value][2]]) for p in list(CornerPosition)]
        self.colors = c
        
    def faceByPosition(self, p):
        for f in self.faces:
            if f.isAt({p}):
                return f
    
    def edgeByPosition(self, p0, p1):
        'Find edge by position'
        for e in self.edges:
            if e.isAt({p0, p1}):
                return e
    
    def cornerByPosition(self, p0, p1, p2):
        'Find edge by position'
        for e in self.corns:
            if e.isAt({p0, p1, p2}):
                return e
    
    def draw(self):

        def center(p):
            return next(iter(self.faceByPosition(p).colors)).name
        
        def edge(p0, p1):
            return self.edgeByPosition(p0, p1).colorAt(p0)

        def cn(p0, p1, p2):
            return self.cornerByPosition(p0, p1, p2).colorAt(p0)
        
        def s__p__c():
            return ' '
        
        u, d, r, l, f, b = Position.U, Position.D, Position.R, Position.L, Position.F, Position.B
        print(f"{s__p__c()} {s__p__c()} {s__p__c()} {cn(u,l,b)} {edge(u,b)} {cn(u,r,b)}")
        print(f'{s__p__c()} {s__p__c()} {s__p__c()} {edge(u,l)} {center(u)} {edge(u,r)}')
        print(f'{s__p__c()} {s__p__c()} {s__p__c()} {cn(u,l,f)} {edge(u,f)} {cn(u,r,f)}')
        print(f'{cn(l,u,b)} {edge(l,u)} {cn(l,f,u)}|{cn(f,u,l)} {edge(f,u)} {cn(f,r,u)}|{cn(r,u,f)} {edge(r,u)} {cn(r,u,b)}|{cn(b,r,u)} {edge(b,u)} {cn(b,u,l)}')
        print(f'{edge(l,b)} {center(l)} {edge(l,f)}|{edge(f,l)} {center(f)} {edge(f,r)}|{edge(r,f)} {center(r)} {edge(r,b)}|{edge(b,r)} {center(b)} {edge(b,l)}')
        print(f'{cn(l,d,b)} {edge(l,d)} {cn(l,d,f)}|{cn(f,d,l)} {edge(f,d)} {cn(f,r,d)}|{cn(r,d,f)} {edge(r,d)} {cn(r,b,d)}|{cn(b,r,d)} {edge(b,d)} {cn(b,d,l)}')
        print(f'{s__p__c()} {s__p__c()} {s__p__c()} {cn(d,f,l)} {edge(d,f)} {cn(d,r,f)}')
        print(f'{s__p__c()} {s__p__c()} {s__p__c()} {edge(d,l)} {center(d)} {edge(d,r)}')
        print(f'{s__p__c()} {s__p__c()} {s__p__c()} {cn(d,b,l)} {edge(d,b)} {cn(d,r,b)}')
        print('')
    
    def moves(self, moves):
        for move in moves:
            self.move(move)
    
    def moveNow(self, colors, move, times):
        
        moves = ['UBDFU', 'UFDBU', 'URDLU', 'ULDRU', 'FLBRF', 'FRBLF']
        key = moves[{"X":0, "X'":1, "Y":2, "Y'":3, "Z":4, "Z'":5, 
                     "R":0, "R'":1, "L":1, "L'":0,
                     "F":2, "F'":3, "B":3, "B'":2,
                     "U":4, "U'":5, "D":5, "D'":4
                     }[move]]
        
        for _ in range(times):
            for color, position in colors.items():
                pos = key.find(position.name)
                if pos >= 0:
                    colors[color] = Position[key[pos + 1]]
    
    def move(self, move, times=1):
        moveLen = len(move)
        if (moveLen not in [1,2] 
            or (moveLen == 2 and move[1] not in ["'", "2"]) 
            or move[0] not in ['X', 'Y', 'Z', 'L', 'R', 'U', 'D', 'F', 'B']):
            return
        if moveLen == 2 and move[1] == '2':
            times = (times * 2) % 4
            move = move[0]
        if times == 0:
            return
        
        for f in (self.faces + self.edges + self.corns):
            if move[0] in ['X', 'Y', 'Z'] or f.isAt({Position[move[0]]}):
                self.moveNow(f.colors, move, times)

def s(string):
    return string.split()

if __name__ == '__main__':
    
    cube = Cube('OGBOWGWGOYBOWOORBGGYYWGBWRRGWOYROWGGWRBYBBRYYRWBRYOBRY')
    cube.draw()
    # Make white plus
    cube.moves(s("L' U' L' U' F U' F U2 R U"))

    # Finish lower cube
    cube.moves(s("X2"))
    cube.moves(s("L' U' L"))
    cube.moves(s("U R U R'"))
    cube.moves(s("Z R U' R'"))
    cube.moves(s("Z L' U' L"))
    cube.moves(s("U R U' R' U2 R U R'"))

    # Finish middle cube
    cube.moves(s("U Z' U R U R' U' F' U' F"))
    cube.moves(s("Z2 L' U' L U F U F'"))
    cube.moves(s("R U R' U' F' U' F Z2 U' L' U' L U F U F'"))
    cube.moves(s("Z2 U R U R' U' F' U' F"))
    
    # Make yello plus
    cube.moves(s("F R U R' U' F'"))
    cube.moves(s("Z2 F R U R' U' F'"))
    cube.moves(s("F R U R' U' F'"))

    # Sune
    cube.moves(s("Z' R U R' U R U2 R'"))

    # Headlights
    cube.moves(s("X R' U R' D2 R U' R' D2 R2"))

    # Solve rest
    cube.moves(s("X' Z"))
    cube.moves(s("R U' R U R U R U' R' U' R2"))
    cube.moves(s("U X2 Z"))
    cube.draw()
