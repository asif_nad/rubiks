import unittest


class Rubik:
    
    def __init__(self, text=''):
        if(text == ''):
            text = 'wwwwwwwwwooooooooogggggggggrrrrrrrrrbbbbbbbbbyyyyyyyyy'
        self.colors = list(text)

    def __str__(self):
        return ''.join(self.colors)
         
    def draw(self):
        c = self.colors
        print(f'      {c[ 0]} {c[ 1]} {c[ 2]}')
        print(f'      {c[ 3]} {c[ 4]} {c[ 5]}')
        print(f'      {c[ 6]} {c[ 7]} {c[ 8]}')
        print(f'{c[ 9]} {c[10]} {c[11]}|{c[18]} {c[19]} {c[20]}|{c[27]} {c[28]} {c[29]}|{c[36]} {c[37]} {c[38]}')
        print(f'{c[12]} {c[13]} {c[14]}|{c[21]} {c[22]} {c[23]}|{c[30]} {c[31]} {c[32]}|{c[39]} {c[40]} {c[41]}')
        print(f'{c[15]} {c[16]} {c[17]}|{c[24]} {c[25]} {c[26]}|{c[33]} {c[34]} {c[35]}|{c[42]} {c[43]} {c[44]}')
        print(f'      {c[45]} {c[46]} {c[47]}')
        print(f'      {c[48]} {c[49]} {c[50]}')
        print(f'      {c[51]} {c[52]} {c[53]}')
        print('')
    
    def face(self,index):
        return self.colors[9*index:9*(index+1)];
    
    def moves(self, moves):
        for move in moves:
            self.move(move)
    
    def move(self, move, times=1):
        times = times % 4
        if move == "X":
            self.rotate([ 0, 44, 45, 18, 1, 43, 46, 19, 2, 42, 47, 20,
                          3, 41, 48, 21, 4, 40, 49, 22, 5, 39, 50, 23,
                          6, 38, 51, 24, 7, 37, 52, 25, 8, 36, 53, 26,
                         27, 29, 35, 33, 28, 32, 34, 30,
                         11, 9, 15, 17, 10, 12, 16, 14], times)
        elif move == "X'":
            self.move("X", 3 * times)
        elif move == "Y":
            for _ in range(times):
                self.move("X"); self.move("Z"); self.move("X'")
        elif move == "Y'":
            self.move("Y", 3);
        elif move == "Z":
            self.rotate([ 9, 36, 27, 18, 10, 37, 28, 19, 11, 38, 29, 20,
                         12, 39, 30, 21, 13, 40, 31, 22, 14, 41, 32, 23,
                         15, 42, 33, 24, 16, 43, 34, 25, 17, 44, 35, 26,
                          0, 2, 8, 6, 1, 5, 7, 3,
                         47, 45, 51, 53, 46, 48, 52, 50], times)
        elif move == "Z'":
            self.move("Z", 3 * times)
        elif move == "F":
            self.rotate([18, 20, 26, 24, 19, 23, 25, 21, 6, 27, 47, 17, 7, 30, 46, 14, 8, 33, 45, 11], times)
        elif move == "F'":
            self.move("F", 3 * times)
        elif move == "B":
            self.move("Z2"); self.move("F", times); self.move("Z2")
        elif move == "B'":
            self.move("B", 3 * times);
        elif move == "L":
            self.move("Z'"); self.move("F", times); self.move("Z")
        elif move == "L'":
            self.move("L", 3 * times)
        elif move == "R":
            self.move("Z"); self.move("F", times); self.move("Z'")
        elif move == "R'":
            self.move("R", 3 * times)
        elif move == "U":  
                self.move("X'"); self.move("F", times); self.move("X")
        elif move == "U'":
            self.move("U", 3 * times)
        elif move == "D":
            self.move("X"); self.move("F", times); self.move("X'")
        elif move == "D'":
            self.move("D", 3)
        elif move[1] == "2":
            self.move(move[0], 2)
            
    def rotate(self, key, times=1):
        """ Rotates the array value based on a group of four as a cube has four sides.
        For every move a minimum of 5 pieces go to next location causing about 20 moves.
        A number of times = 4 as same is not moving at all.
        
        Args:
            key (int array): contains group of 4 numbers where 1 goes to 2, 2 goes to 3, 3 goes to 4 and 4 goes to 1
            times (int): the number of times the move is to be done 
        """
        times = times % 4
        if times == 0:  # This method will only work if times is 1, 2, or 3
            return
        for index in range(0, len(key), 4):
            coords = key[index:index + 4]
            for _ in range(0, times):
                tmp = self.colors[coords[3]]
                self.colors[coords[3]] = self.colors[coords[2]]
                self.colors[coords[2]] = self.colors[coords[1]]
                self.colors[coords[1]] = self.colors[coords[0]]
                self.colors[coords[0]] = tmp
    
    
class RubikTest(unittest.TestCase):
    
    def test_upper(self):
        self.assertEqual('food'.upper(), 'FOO')
    
        
if __name__ == '__main__':
    # key = 'rbygwbwrwgobroobbyoyoygygrygrggrgbyyoowwbgrowrwowybrwb'
    key = 'ogbowgwgoybowoorbggyywgbwrrgwoyrowggwrbybbryyrwbryobry'
    solkey = 'wwwwwwwwwooooooooogggggggggrrrrrrrrrbbbbbbbbbyyyyyyyyy'
    move = "Y"
    cube = Rubik(key)
    cube.draw()
    # Make white plus
    cube.moves(["L'", "U'", "L'", "U'", "F", "U'", "F", "U2", "R", "U"])
    # Move white to bottom
    cube.move("X2")
    # Solve white corners
    cube.moves(["L'", "U'", "L"])
    cube.moves(["U", "R", "U", "R'"])
    cube.moves(["Z2", "L'", "U2", "L", "U2", "L'", "U'", "L"])
    cube.moves(["D'", "L'", "U'", "L", "D"])
    # Solve side edges
    cube.moves(["R", "U", "R'", "U'", "F'", "U'", "F"])
    cube.moves(["L'", "U'", "L", "U", "F", "U", "F'"])
    cube.moves(["Z", "R", "U", "R'", "U'", "F'", "U'", "F", "Z2"])
    cube.moves(["U'", "L'", "U'", "L", "U", "F", "U", "F'", "Z2"])
    cube.moves(["U", "R", "U", "R'", "U'", "F'", "U'", "F", "Z'"])
    # Make a yellow plus
    cube.moves(["F", "R", "U", "R'", "U'", "F'"])
    cube.moves(["F", "R", "U", "R'", "U'", "F'"])
    # Bring all yellows on top
    cube.moves(["R", "U2", "R'", "U'", "R", "U'", "R'"])
    # Solve headlight 
    cube.moves(["X", "R'", "U", "R'", "D2", "R", "U'", "R'", "D2", "R2", "B2"])
    # Solve last
    cube.moves(["X'", "Z2"])
    cube.moves(["L'", "U", "L'", "U'", "L'", "U'", "L'", "U", "L", "U", "L2"])
    cube.moves(["X2"])
    cube.draw()
    print(cube.face(0))

    print(cube)
    print(str(cube) == solkey)
